#  SNAKE GAME  :snake:

  Famoso "[jogo da cobrinha](https://lucasrmagalhaes.github.io/snake-js/ "jogo da cobrinha")" desenvolvido em JavaScript.

[![SNAKE GAME](https://i.imgur.com/3eKgtYO.jpg "SNAKE GAME")](https://i.imgur.com/3eKgtYO.jpg "SNAKE GAME")


------------



- Updates:
1. Alterado a cor de background.
2. Adicionado espaçamento entre os quadrados da cobrinha.

------------



- Desafio prático realizado na plataforma [Digital Innovation One](https://web.digitalinnovation.one/home "Digital Innovation One"): [Recriando o jogo da cobrinha com JavaScript](https://web.digitalinnovation.one/course/desafio-pratico-recriando-o-jogo-da-cobrinha-com-javascript/learning/66d83831-bae1-45f7-b2ea-af7d64d5d4f5?back=/track/desenvolvedor-front-end-reactjs&bootcamp_id=abf8f19f-691b-4dac-a14a-11ddcf3a14cd "Recriando o jogo da cobrinha com JavaScript").
